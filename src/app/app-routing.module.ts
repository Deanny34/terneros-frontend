import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListaTernerosComponent } from './componentes/lista-terneros/lista-terneros.component';
import { RegistrarCriaComponent } from './componentes/registrar-cria/registrar-cria.component';
import { ActualizarCriaComponent } from './componentes/actualizar-cria/actualizar-cria.component';
import { CuarentenaComponent } from './componentes/cuarentena/cuarentena.component';
import { RegistroCuarentenaComponent } from './componentes/registro-cuarentena/registro-cuarentena.component';

const routes: Routes = [
  {path:"terneros", component:ListaTernerosComponent},
  {path:"", redirectTo:"terneros", pathMatch:"full"},
  {path:"registrar-ternero", component:RegistrarCriaComponent},
  {path:"actualizar-ternero/:id", component:ActualizarCriaComponent},
  {path:"detalles-terneros", component:CuarentenaComponent},
  {path:"datos-sensores/:id", component:RegistroCuarentenaComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

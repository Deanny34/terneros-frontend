import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EjemploComponent } from './componentes/ejemplo/ejemplo.component';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ListaTernerosComponent } from './componentes/lista-terneros/lista-terneros.component';
import { RegistrarCriaComponent } from './componentes/registrar-cria/registrar-cria.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ActualizarCriaComponent } from './componentes/actualizar-cria/actualizar-cria.component';
import { RegistroCuarentenaComponent } from './componentes/registro-cuarentena/registro-cuarentena.component';
import { CuarentenaComponent } from './componentes/cuarentena/cuarentena.component';

@NgModule({
  declarations: [
    AppComponent,
    EjemploComponent,
    ListaTernerosComponent,
    RegistrarCriaComponent,
    ActualizarCriaComponent,
    CuarentenaComponent,
    RegistroCuarentenaComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

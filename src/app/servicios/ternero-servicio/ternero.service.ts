import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Ternero } from 'src/app/modelos/ternero';

@Injectable({
  providedIn: 'root'
})
export class TerneroService {

  private baseURL = "http://localhost:8080/api/v1/";
  
  constructor(private HttpClient : HttpClient) { }

  obtenerListaTerneros():Observable<Ternero[]>{
    return this.HttpClient.get<Ternero[]>(`${this.baseURL}terneros`);
  }

  obtenerTerneroId(id:number) : Observable<Ternero> {
    return this.HttpClient.get<Ternero>(`${this.baseURL}terneros/${id}`);
  }

  registrarTernero(ternero:Ternero) : Observable<Object> {
    return this.HttpClient.post(`${this.baseURL}terneros`, ternero);
  }

  actualizarTernero(id:number,ternero:Ternero):Observable<Object> {
    return this.HttpClient.put(`${this.baseURL}terneros/${id}`,ternero);
  }

  actualizarTerneroSensores(id:number,ternero:Ternero):Observable<Object> {
    return this.HttpClient.put(`${this.baseURL}ternero-sensores/${id}`,ternero);
  }

  actualizarTerneroCuarentena(id:number,ternero:Ternero):Observable<Object> {
    return this.HttpClient.put(`${this.baseURL}ternero-cuarentena/${id}`,ternero);
  }
}

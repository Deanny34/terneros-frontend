import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CuarentenaComponent } from './cuarentena.component';

describe('CuarentenaComponent', () => {
  let component: CuarentenaComponent;
  let fixture: ComponentFixture<CuarentenaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CuarentenaComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CuarentenaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

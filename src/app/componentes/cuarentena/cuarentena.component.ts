import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Ternero } from 'src/app/modelos/ternero';
import { TerneroService } from 'src/app/servicios/ternero-servicio/ternero.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-cuarentena',
  templateUrl: './cuarentena.component.html',
  styleUrls: ['./cuarentena.component.css']
})
export class CuarentenaComponent implements OnInit{
  terneros:Ternero[];
  id:number;

  constructor(private terneroServicio:TerneroService, private router:Router) { }

  ngOnInit(): void {
      this.obtenerTerneros();
  }

  private obtenerTerneros() {
    this.terneroServicio.obtenerListaTerneros().subscribe(dato => {
      this.terneros = dato;
    });
  }

  redireccionarDatosSensores(id:number) {
    this.router.navigate(["/datos-sensores/", id]);
  }

  activarCuarentena(ternero:Ternero, ) {
    let mensaje:string = "";
    let cuarentena:number;
    if(ternero.cuarentena == 1) {
      mensaje = "desactivar";
      cuarentena = 0;
    } else if (ternero.cuarentena == 0) {
      mensaje = "activar";
      cuarentena = 1;
    }
    Swal.fire({
      title: "¿Desea " + mensaje + " la cuarentena para el ternero?",
      html: "[<b>" + ternero.id + "</b>:  " + ternero.nombre + "]",
      showDenyButton: true,
      icon:'question',
      showCancelButton: true,
      confirmButtonText: "Si",
      denyButtonText: `No`
    }).then((result) => {
      if (result.isConfirmed) {
        ternero.cuarentena = cuarentena;
        this.guardarTernero(ternero);
      } else if (result.isDenied) {
        Swal.fire("Se canceló la operación", "", "info");
      }
    });
  }

  guardarTernero(ternero:Ternero) {
    this.terneroServicio.actualizarTerneroSensores(ternero.id, ternero).subscribe(
      dato => {
        Swal.fire({
          position: "top-end",
          icon: "success",
          title: "Registro exitoso",
          showConfirmButton: false,
          timer: 1500
        });
      },
      error => console.log(error)
    );
  }

  checarTernero(ternero:Ternero) {
    Swal.fire({
      title: "¿Cuál es el estado de salud de la cría?",
      html: "<p>Temperatura: <b>" + ternero.temperatura + "</b> <br>Frecuencia cardíaca: <b>" + ternero.frecuencia_cardiaca + "</b><br>Frecuencia respiratoria: <b>" + ternero.frecuencia_respiratoria + "</b><br>Presión sanguínea: <b>" + ternero.presion_sanguinea + "</b></p>",
      icon: 'question',
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: "Sano",
      denyButtonText: `Cuarentena`
    }).then((result) => {
      if (result.isConfirmed) {
        ternero.cuarentena = 0;
      } else if (result.isDenied) {
        ternero.cuarentena = 1;
      }
      this.guardarTernero(ternero);
    });
  }
}

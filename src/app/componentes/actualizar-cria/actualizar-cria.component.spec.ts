import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActualizarCriaComponent } from './actualizar-cria.component';

describe('ActualizarCriaComponent', () => {
  let component: ActualizarCriaComponent;
  let fixture: ComponentFixture<ActualizarCriaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActualizarCriaComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ActualizarCriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

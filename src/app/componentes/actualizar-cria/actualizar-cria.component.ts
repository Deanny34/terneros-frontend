import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Ternero } from 'src/app/modelos/ternero';
import { TerneroService } from 'src/app/servicios/ternero-servicio/ternero.service';
import Swal from 'sweetalert2';
import { RegistrarCriaComponent } from '../registrar-cria/registrar-cria.component';

@Component({
  selector: 'app-actualizar-cria',
  templateUrl: './actualizar-cria.component.html',
  styleUrls: ['./actualizar-cria.component.css']
})
export class ActualizarCriaComponent implements OnInit {

  id:number;
  ternero:Ternero = new Ternero();
  registrarCriaComponent:RegistrarCriaComponent;

  constructor(private terneroService:TerneroService, private router:Router, private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.obtenerTernero();
  }

  redireccioniarListaTerneros() {
    this.router.navigate(["/terneros"]);
    Swal.fire({
      position: "top-end",
      icon: "success",
      title: "Registro exitoso",
      showConfirmButton: false,
      timer: 1500
    });
  }

  onSubmit() {
    this.actualizarTernero(this.ternero);
  };

  actualizarTernero(ternero:Ternero) {
    ternero.tipo_grasa = this.tipoGrasaTernero(ternero);
    this.terneroService.actualizarTernero(ternero.id, ternero).subscribe(
      dato => {
        this.redireccioniarListaTerneros();
      },
      error => console.log(error)
    );
  }

  tipoGrasaTernero(ternero:Ternero) {
    let tipo1:number = 0;
    let tipo2:number = 0;
    if (ternero.peso >= 15 && ternero.peso <= 25) {
      tipo1++;
    } else if (ternero.peso < 15 || ternero.peso > 25) {
      tipo2++;
    }

    if (ternero.color_musculo >= 3 && ternero.color_musculo <= 5) {
      tipo1++;
    } else if (ternero.color_musculo < 3 || ternero.color_musculo > 5) {
      tipo2++;
    }

    if (ternero.calidad_marmoleo < 3) {
      tipo1++;
    } else if ( ternero.calidad_marmoleo >= 3) {
      tipo2++;
    }

    // tipo 1 = 1 | tipo 2 = 2 | indeterminado = 0
    if (tipo1 >= 2) {
      return 1;
    } else if (tipo2 >= 2) {
      return 2;
    } else {
      return 0;
    }
  }

  private obtenerTernero() {
    this.terneroService.obtenerTerneroId(this.id).subscribe(
      dato => {
        this.ternero = dato;
      },
      error => console.log(error)
    );
  }
}

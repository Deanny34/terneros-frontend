import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/servicios/api.service';

@Component({
  selector: 'app-ejemplo',
  templateUrl: './ejemplo.component.html',
  styleUrls: ['./ejemplo.component.css']
})
export class EjemploComponent implements OnInit {
  saludo: string = '';

  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
    this.apiService.obtenerSaludo().subscribe(
      data => {
        console.log("Respuesta del servidor: ", data);
        this.saludo = data;
      },
      error => {
        console.error("Error al obtener el saludo: ", error);
      }
    );
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrarCriaComponent } from './registrar-cria.component';

describe('RegistrarCriaComponent', () => {
  let component: RegistrarCriaComponent;
  let fixture: ComponentFixture<RegistrarCriaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegistrarCriaComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RegistrarCriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Ternero } from 'src/app/modelos/ternero';
import { TerneroService } from 'src/app/servicios/ternero-servicio/ternero.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-registrar-cria',
  templateUrl: './registrar-cria.component.html',
  styleUrls: ['./registrar-cria.component.css']
})
export class RegistrarCriaComponent implements OnInit {

  ternero: Ternero = new Ternero();
  
  constructor(private terneroServicio:TerneroService, private router:Router) { }

  ngOnInit(): void { }

  guardarTernero() {
    if (Object.keys(this.ternero).length === 0) {
      Swal.fire({
        position: "top-end",
        icon: "warning",
        title: "Favor de llenar todos los campos",
        showConfirmButton: false,
        timer: 1500
      });
    } else {
      this.ternero.tipo_grasa = this.tipoGrasaTernero(this.ternero);
      this.terneroServicio.registrarTernero(this.ternero).subscribe(
        dato => {
          console.log(dato);
          this.redireccionarListaTerneros();
        },
        error => console.log(error)
      );
    }
  }

  redireccionarListaTerneros() {
    this.router.navigate(["/terneros"]);
  }

  onSubmit() {
    this.guardarTernero();
  }

  tipoGrasaTernero(ternero:Ternero) {
    let tipo1:number = 0;
    let tipo2:number = 0;
    if (ternero.peso >= 15 && ternero.peso <= 25) {
      tipo1++;
    } else if (ternero.peso < 15 || ternero.peso > 25) {
      tipo2++;
    }

    if (ternero.color_musculo >= 3 && ternero.color_musculo <= 5) {
      tipo1++;
    } else if (ternero.color_musculo < 3 || ternero.color_musculo > 5) {
      tipo2++;
    }

    if (ternero.calidad_marmoleo < 3) {
      tipo1++;
    } else if ( ternero.calidad_marmoleo >= 3) {
      tipo2++;
    }

    // tipo 1 = 1 | tipo 2 = 2 | indeterminado = 0
    if (tipo1 >= 2) {
      return 1;
    } else if (tipo2 >= 2) {
      return 2;
    } else {
      return 0;
    }
  }

}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Ternero } from 'src/app/modelos/ternero';
import { TerneroService } from 'src/app/servicios/ternero-servicio/ternero.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-registro-cuarentena',
  templateUrl: './registro-cuarentena.component.html',
  styleUrls: ['./registro-cuarentena.component.css']
})
export class RegistroCuarentenaComponent implements OnInit {

  ternero: Ternero = new Ternero();
  id:number;
  currentDate: Date = new Date();
  anio = this.currentDate.getFullYear();
  mes = (this.currentDate.getMonth() + 1).toString().padStart(2, '0');
  dia = this.currentDate.getDate().toString().padStart(2, '0');
  fechaActualFormateada = `${this.anio}-${this.mes}-${this.dia}`;
  actualDate: Date = new Date(this.fechaActualFormateada);

  constructor(private terneroService:TerneroService, private router:Router, private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.obtenerTernero();
  }

  redireccionarDetallesTerneros() {
    this.router.navigate(["/detalles-terneros"]);
    Swal.fire({
      position: "top-end",
      icon: "success",
      title: "Registro exitoso",
      showConfirmButton: false,
      timer: 1500
    });
  }

  onSubmit() {
    this.guardarTerneroSensores();
  }

  guardarTerneroSensores() {
    if (Object.keys(this.ternero).length === 0) {
      Swal.fire({
        position: "top-end",
        icon: "warning",
        title: "Favor de llenar todos los campos",
        showConfirmButton: false,
        timer: 1500
      });
    } else {
      console.log(this.fechaActualFormateada);
      this.ternero.fecha_registro_sensores = this.fechaActualFormateada;
      this.ternero.cuarentena = this.statusTernero(this.ternero);
      this.terneroService.actualizarTerneroSensores(this.id, this.ternero).subscribe(
        dato => {
          console.log(dato);
          this.redireccionarDetallesTerneros();
        },
        error => console.log(error)
      );
    }
  }

  statusTernero(ternero:Ternero) {
    let saludable:number = 0;
    let enfermo:number = 0;
    if (ternero.temperatura >= 37.5 && ternero.temperatura <= 39.5) {
      saludable++;
    } else if (ternero.temperatura > 39.5) {
      enfermo++;
    }

    if (ternero.frecuencia_cardiaca >= 70 && ternero.frecuencia_cardiaca <= 80) {
      saludable++;
    } else if (ternero.frecuencia_cardiaca < 70 || ternero.frecuencia_cardiaca > 80) {
      enfermo++;
    }

    if (ternero.frecuencia_respiratoria >= 15 && ternero.frecuencia_respiratoria <= 20) {
      saludable++;
    } else if (ternero.frecuencia_respiratoria < 15 || ternero.frecuencia_respiratoria > 20) {
      enfermo++;
    }

    if (ternero.presion_sanguinea >= 8 && ternero.presion_sanguinea <= 10) {
      saludable++;
    } else if (ternero.presion_sanguinea > 10) {
      enfermo++;
    }

    // saludable = 0 | enfermo = 1 | indeterminado = 2
    if (saludable >= 3) {
      return 0;
    } else if (enfermo >= 3) {
      return 1;
    } else {
      return 2;
    }
  }

  private obtenerTernero() {
    this.terneroService.obtenerTerneroId(this.id).subscribe(
      dato => {
        this.ternero = dato;
      },
      error => console.log(error)
    );
  }
}

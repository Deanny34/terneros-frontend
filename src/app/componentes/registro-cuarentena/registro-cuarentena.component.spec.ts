import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistroCuarentenaComponent } from './registro-cuarentena.component';

describe('RegistroCuarentenaComponent', () => {
  let component: RegistroCuarentenaComponent;
  let fixture: ComponentFixture<RegistroCuarentenaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegistroCuarentenaComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RegistroCuarentenaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { Ternero } from 'src/app/modelos/ternero';
import { TerneroService } from 'src/app/servicios/ternero-servicio/ternero.service';
import { Router } from "@angular/router";

@Component({
  selector: 'app-lista-terneros',
  templateUrl: './lista-terneros.component.html',
  styleUrls: ['./lista-terneros.component.css']
})
export class ListaTernerosComponent implements OnInit {

  terneros:Ternero[];
  id:number;

  constructor(private terneroServicio:TerneroService, private router:Router) { }

  ngOnInit(): void {
    this.obtenerTerneros();
  }

  actualizarTernero(id:number) {
    this.router.navigate(["actualizar-ternero", id]);
  }

  private obtenerTerneros() {
    this.terneroServicio.obtenerListaTerneros().subscribe(dato => {
      this.terneros = dato;
    });
  }
}

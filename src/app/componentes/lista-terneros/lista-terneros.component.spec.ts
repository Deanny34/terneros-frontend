import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaTernerosComponent } from './lista-terneros.component';

describe('ListaTernerosComponent', () => {
  let component: ListaTernerosComponent;
  let fixture: ComponentFixture<ListaTernerosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListaTernerosComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ListaTernerosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

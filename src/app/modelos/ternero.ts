export class Ternero {
    id:number;
    nombre:string;
    peso:number;
    costo:number;
    proveedor:string;
    descripcion:string;
    color_musculo:number;
    calidad_marmoleo:number;
    tipo_grasa:number;
    frecuencia_cardiaca:number;
    frecuencia_respiratoria:number;
    presion_sanguinea:number;
    temperatura:number;
    fecha_registro_sensores:string;
    cuarentena:number;
}